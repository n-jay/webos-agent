var pkgInfo = require('./package.json');
var Service = require('webos-service');
var service = new Service(pkgInfo.name);
var greeting = "Hello, World!";

// a method that always returns the same value
service.register("hello", function(message) {
	console.log("In hello callback");
	message.respond({
		returnValue: true,
		message: greeting
	});
});

// Service that requests server for operation
service.register("operation", function(message) {
	var counter = 0
	setInterval(operationRequest, 10000)
	
	function operationRequest() {
		console.log("In hello callback");
		
		
		message.respond({
			returnValue: true,
			message: counter + " Service active " + message.payload.msg + " | " + message.payload.endpoint
		});
		counter++;
		
		
		/** This is not functional atm, fix is being worked on
		$.ajax({
            type: "GET",
            url: message.payload.endpoint + "/api/device-mgt/v1.0/device/agent/pending/operations/webOS/18",
            headers: {
            	'Authorization': 'Bearer ' +  message.payload.msg,
                'Content-Type': 'application/json'
            },
            success: function (resp) {
            	message.respond({
        			returnValue: true,
        			message: resp
        		});
            },
            error: function (xhr) {
            	message.respond({
        			returnValue: false,
        			message: "Fail" + JSON.parse(xhr)
        		});
            }
        });
        **/
	}
});




