# Welcome to webOS TV Device Management agent

<a href='https://opensource.org/licenses/Apache-2.0'><img src='https://img.shields.io/badge/License-Apache%202.0-blue.svg'></a>

This Device Management Agent allows you to authenticate and enroll your webOS TV in Entgra IoT Server.

An architecture diagram of the webapp is provided in the repo.

### webOS TV Device Management Agent current features

- Enroll webOS TV in server
- View details of webOS TV device
- Initiates background service on enrolment success

### webOS TV Device Management Agent functionality under immediate development

- Send AJAX calls from within background webOS service
- Check if background service persists upon app close (using Beanvisor tool)

### webOS TV Device Management Agent features planned for development

- Control volume
- View currently running app state 

### Video Tutorial

A walkthrough video going through the Device Type setup on Entgra IoTS and enrollment can be found [here.](https://drive.google.com/open?id=13-ZIAcDs_6CHY8KwJAJRTu7ZR9nMxyyX)

### Documentation

A quick tutorial document on the SDK installation and app setup is being written and will be uploaded soon.

### Contact

Contact developer [at this link.](http://forum.webosose.org/u/n-jay/)
